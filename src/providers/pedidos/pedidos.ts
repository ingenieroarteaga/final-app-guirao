import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GLOBAL } from "../../app/global";
import {LoadingController, Platform } from 'ionic-angular';
import { Observable } from 'rxjs';
import { Pedido } from '../../pages/pedidos/pedido';
import {TokenProvider}  from "../token/token";

/*
  Generated class for the PedidosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PedidosProvider {

  private url: string =GLOBAL.urlPedidos;
  //creo variables para enviar cabecera con token
  authHeader: any;
  headersObj = new Headers();


  constructor(public http: HttpClient, private token:TokenProvider) {
    console.log('Hello PedidosProvider Provider');
  }


  // create(pedidoVenta: Pedido): Observable<Pedido> {
  //   console.log(this.url);
  //   console.log(pedidoVenta);
  //   return this.http.post<Pedido>(this.url, pedidoVenta, { headers: this.httpHeaders });
  //
  // }

  create(pedidoVenta) {
  //obtengo el token
  this.authHeader = this.token.getToken();
  //asigno el token a la cabecera
  let headers = new HttpHeaders({
    'Content-Type': 'application/json' ,
    'Authorization': 'Bearer ' + this.authHeader
  });
  return new Promise((resolve, reject) => {
    this.http.post(this.url, pedidoVenta, {headers})
      .subscribe(res => {
        resolve(res);
      }, (err) => {

      });
  });
}



  update(pedidoVenta: Pedido): Observable<Pedido> {
    //obtengo el token
    this.authHeader = this.token.getToken();
    //asigno el token a la cabecera
    let headers = new HttpHeaders({
      'Content-Type': 'application/json' ,
      'Authorization': 'Bearer ' + this.authHeader
    });
    
    return this.http.put<Pedido>(this.url , pedidoVenta, { headers});
  }

}
