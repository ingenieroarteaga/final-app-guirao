import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { GLOBAL } from '../../app/global';
import { map } from 'rxjs/operators';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  public url:string = GLOBAL.url;

  // baseUrl: 'http://192.168.0.110:8080/email2sms/';
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) {


  }


  attemptAuth(username: string, password: string): Observable<any> {
    const credentials = { username: username, password: password };
    // this.url = 'http://185.37.226.154:8080/guirao/';
    return this.http.post<any>(this.url + 'token/generate-token', credentials);

  }



  }
