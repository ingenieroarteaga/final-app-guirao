import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GLOBAL } from "../../app/global";
import {LoadingController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage/esm5';
import {Pedido} from '../../pages/pedidos/pedido';
import {TokenProvider}  from "../token/token";
/*
  Generated class for the ResumenProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ResumenProvider {

  path: string = GLOBAL.urlPedidosVentas;
  loading:any;
  //creo variables para enviar cabecera con token
  authHeader: any;
  headersObj = new Headers();

  constructor(public http: HttpClient, public loadingCtrl: LoadingController,
              private platform:Platform, private storage: Storage, private token: TokenProvider) {

    console.log('Stocks provider');

  }

  //Obtengo la lista de pedidos
  getPedidosVentas() {
    //obtengo el token
    this.authHeader = this.token.getToken();
    //asigno el token a la cabecera
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.authHeader
    });
    return this.http.get<Pedido[]>(this.path, {headers});
  }


}
