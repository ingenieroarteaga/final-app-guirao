import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GLOBAL } from "../../app/global";
import {LoadingController, Platform, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage/esm5';
import {TokenProvider}  from "../token/token";
/*
  Generated class for the FamiliaProductosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FamiliaProductosProvider {

    familias: any = [];
    path: string = GLOBAL.urlFamilias;
    loading:any;
    //creo variables para enviar cabecera con token
    authHeader: any;
    headersObj = new Headers();

    constructor(public http: HttpClient, public loadingCtrl: LoadingController,
                private platform:Platform, private storage: Storage,
                private token: TokenProvider, public alertCtrl: AlertController) {
      console.log('Hello FamiliaProductosProvider');
    }

    // obtengo la lista de productos actualizada
    primeraCarga() {

      // Si ya existe un archivo guardado, trabajo con ese hasta que se termine de bajar el nuevo
      //uso this.storage para mobile y localStorage para Desktop
      // if (localStorage.getItem("familias")) {
      this.storage.get("familias").then((data)=> {
        if (data != undefined) {
          // // Creo el loading
           this.loading =  this.loadingCtrl.create({
              content: 'Cargando datos locales...',
           });
            // muestro el loading
           this.loading.present();

           console.log("uso familias guardada mientras descargo");
           this.cargarStorage();
           this.loading.dismiss();

           console.log('ya cargue el guardado pero igual descargo el actuazalizado');
           this.getFamilias()
               .subscribe(data => {
                   this.familias = data;
                   console.log('ya guarde e iguale el actualizado');
                   // envio notificación de actualizacióna
                   const alert = this.alertCtrl.create({
                      title: 'Familias actualizados',
                      subTitle: 'La lista de familias ha sido actualizada',
                      buttons: ['OK']
                    });

                   //envio la lista actualizada para almacenar
                   this.guardarStorage();


                  alert.present();

               },);

        }  else {

          // // Creo el loading
          this.loading =  this.loadingCtrl.create({
              content: 'Cargando datos...',
          });
          // //muestro el loadigng
          this.loading.present();
          //obtengo los productos
          this.getFamilias()
            .subscribe(data => {
               console.log(data);
               this.familias = data;
               //cierro loading
               this.loading.dismiss();
               //envio la lista actualizada para almacenar
               this.guardarStorage();
          });

        }
      }); // agrego parentesis par mobiel y lo sacao para escritorio

     }


    // Utilizo está función para traer los datos del servidor
    getFamilias() {
      //obtengo el token
      this.authHeader = this.token.getToken();
      //asigno el token a la cabecera
      let headers = new HttpHeaders({
        'Authorization': 'Bearer ' + this.authHeader
      });
      //realizo la peticíon
      return this.http.get(this.path, {headers});
    }



    // Sino tengo internet le indico a la app que use la
    // ultima lista de prouctos que descargamos.
    // Compruebo si estoy almacenando en compu o en celu
    cargarStorage() {

      let promesa = new Promise((resolve, reject)=> {

        if (this.platform.is("cordova")) {

          console.log("inicializando storage mobile")
          this.storage.ready()
                      .then(()=>{

                        this.storage.get("familias")
                          .then( familias => {
                            this.familias = familias;
                            console.log("uso familias ya guardado mobile");
                            resolve ();
                          } )
                      })
        } else {

          if (localStorage.getItem("familias")) {

             this.familias = JSON.parse(localStorage.getItem("familias"));
             console.log("uso el que ya está guardado desktop");
             resolve ();

          } else {
            console.log("no hay archivo guardado");
             this.primeraCarga();
             resolve ();
          }

        }

      });
      return promesa;

    }

    // Guardo la lista de productos actualizada.
    guardarStorage() {

      if (this.platform.is("cordova")) {
        this.storage.ready()
                    .then(()=>{

                      this.storage.set("familias", this.familias);
                      console.log("se guardo el archivo de familias en mobile.");

                    })

      } else {
        // este codigo sirve para guardar en local desktop, pero
        // lo desactivo porque si guardo cliente y productos excedo
        // el espacio que me dan los browser para almacentar
        localStorage.setItem("familias", JSON.stringify(this.familias) );
        console.log("se guardo el archivo de familias en desktop.");
      }
    }
  }
