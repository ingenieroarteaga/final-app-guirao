import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AjustesModalUsuarioPage } from './ajustes-modal-usuario';

@NgModule({
  declarations: [
    AjustesModalUsuarioPage,
  ],
  imports: [
    IonicPageModule.forChild(AjustesModalUsuarioPage),
  ],
})
export class AjustesModalUsuarioPageModule {}
