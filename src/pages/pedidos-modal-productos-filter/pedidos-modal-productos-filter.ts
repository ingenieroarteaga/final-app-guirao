import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,Searchbar } from 'ionic-angular';
import {StockProvider} from "../../providers/stock/stock";
import { AlertController } from 'ionic-angular';

import { ListaProductos } from '../productos/listaProductos';
import { Stock } from '../pedidos/stock';
import { Platform, LoadingController } from 'ionic-angular';
import {TipoProductoProvider} from "../../providers/tipo-producto/tipo-producto";
import {MarcasProvider} from "../../providers/marcas/marcas";
import {FamiliaProductosProvider} from "../../providers/familia-productos/familia-productos";
//Para almacenar información en el dispositivo
import { Storage } from '@ionic/storage/esm5';


@IonicPage()
@Component({
  selector: 'page-pedidos-modal-productos-filter',
  templateUrl: 'pedidos-modal-productos-filter.html',
})
export class PedidosModalProductosFilterPage {

   // para almacenar listas completas
  tipos: any = [];
  familias: any = [];
  marcas: any = [];
  // para almacenar los filtros elegidos
  filtroMarca: string = '';
  filtroTipoProducto: string = '';
  filtroFamiliaProducto: string = '';
  filtros: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public stocksProvider: StockProvider, public tiposProvider: TipoProductoProvider,
              public familiasProvider: FamiliaProductosProvider, public storage: Storage, public platform: Platform,
              public viewCtrl: ViewController, public marcasProdvider: MarcasProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PedidosModalProductosFilterPage');
    this.cargarFiltrosIniciales();
  }

  cargarFiltrosIniciales() {

    // cargo los datos de tipos de producto guardados en storage
    let promesaTipo = new Promise((resolve, reject)=> {
      // compruebo si es mobile o desktop
      if (this.platform.is("cordova")) {
        this.storage.ready()
                   .then(()=>{
                    console.log("cargo los tipos del storage");
                    this.storage.get("tipos")
                      .then( tipos=> {
                        this.tipos = tipos;
                        console.log("tipos cargados");
                      })
                  });
        resolve ();
      } else {
        this.tipos = JSON.parse(localStorage.getItem("tipos"));
        console.log("se cargaron los tipos", this.tipos);
        resolve ();
      }
    });

    // cargo los datos de familias de producto guardados en storage
    let promesaFamilia = new Promise((resolve, reject)=> {
      // compruebo si es mobile o desktop
      if (this.platform.is("cordova")) {
        this.storage.ready()
                   .then(()=>{
                    console.log("cargo las familias del storage");
                    this.storage.get("familias")
                      .then( familias => {
                        this.familias = familias;
                        console.log("familias cargadas");
                      })
                  });
        resolve ();
      } else {
        this.familias = JSON.parse(localStorage.getItem("familias"));
        console.log("se cargaron las familias", this.familias);
        resolve ();
      }
    });

    // cargo los datos de marcas de producto guardados en storage
    let promesaMarcas = new Promise((resolve, reject)=> {
      // compruebo si es mobile o desktop
      if (this.platform.is("cordova")) {
        this.storage.ready()
                   .then(()=>{
                    console.log("cargo las marcas del storage");
                    this.storage.get("marcas")
                      .then( marcas => {
                        this.marcas = marcas;
                        console.log("marcas cargadas");
                      })
                  });
        resolve ();
      } else {
        this.marcas = JSON.parse(localStorage.getItem("marcas"));
        console.log("se cargaron las marcas", this.marcas);
        resolve ();
      }
    });
  }

  //cuando cierro el modal sin elegir producto
  cerrarModal() {

    // this.viewCtrl.dismiss( {stock: null , productoPrecio: null , cantidad: null ,} );
    this.filtros.push(this.filtroMarca);
    this.filtros.push(this.filtroTipoProducto);
    this.filtros.push(this.filtroFamiliaProducto);

    this.viewCtrl.dismiss(this.filtros);

  }
}
