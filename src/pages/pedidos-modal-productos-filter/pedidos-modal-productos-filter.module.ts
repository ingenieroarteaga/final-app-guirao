import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidosModalProductosFilterPage } from './pedidos-modal-productos-filter';

@NgModule({
  declarations: [
    PedidosModalProductosFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(PedidosModalProductosFilterPage),
  ],
})
export class PedidosModalProductosFilterPageModule {}
