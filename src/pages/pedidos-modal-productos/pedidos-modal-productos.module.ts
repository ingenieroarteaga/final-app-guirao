import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidosModalProductosPage } from './pedidos-modal-productos';

@NgModule({
  declarations: [
    PedidosModalProductosPage,
  ],
  imports: [
    IonicPageModule.forChild(PedidosModalProductosPage),
  ],
})
export class PedidosModalProductosPageModule {}
