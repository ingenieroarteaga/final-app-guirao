import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


export class Producto {

    public id: number;
    public modelo: string;  //m
    public descripcion: string;//m
    public codigo: string;  //m
    public codBarra: string;//111 8
    public precioCompra: number;// 222 3
    public ganancia: number; // 222 3
    public presentacion: string; //111  4
    public fechaBaja: Date;
    public stockList: any;
    public familiaProd: any;//5sss
    public marca: any;//2sss
    public proveedor: any;
    public rubro: any; //1sss
    public tipoProducto: any;//3sss
    // public concepto: Concepto;
    public iva: any;//4selectsss

}
