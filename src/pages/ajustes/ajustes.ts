import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import {AjustesModalUsuarioPage}  from "../ajustes-modal-usuario/ajustes-modal-usuario";

/**
 * Generated class for the AjustesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ajustes',
  templateUrl: 'ajustes.html',
})
export class AjustesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public modalCtrl: ModalController,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AjustesPage');
  }

  //Abro el modal para elegir cliente
  openModalUsuario() {

    let chooseModalCliente = this.modalCtrl.create(AjustesModalUsuarioPage);
      chooseModalCliente.onDidDismiss(data => {

    });
    chooseModalCliente.present();

  }

}
