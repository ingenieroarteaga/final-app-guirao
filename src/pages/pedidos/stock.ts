//EN EL FRONT DE LA APP HAY UN MANEJO DE STOCK PARA REALIZAR PEDIDOS
//EN LA APP NO ESTÁ ESE MANEJO, PERO EL OBJETO QUE ENVIO TIENE QUE TENER
//COMO UNO DE LOS ATRIBUTOS "STOCK" Y DENDTRO DE STOCK ESTÁ LA INFO DEL
//PRODUCTO QUE QUEREMOS AGREGAR A LA LSITA

import { Producto } from "../productos/producto";
import { ListaProductos } from '../productos/listaProductos';

export class Stock {
    public id: number;
    public cantidadMinima: number;
    public cantidad: number;
    //public presupuestoDetalleList: PresupuestoDetalle[];
    public producto: Producto;
    public sucursal: any;
    //public movimientoStockList: MovimientoStock[];
    //public pedidoCompraDetalleList: PedidoCompraDetalle[];
    public pedidoEntregaDetalleList: ListaProductos[];


}
