import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListaProductos } from '../productos/listaProductos';
import { Cliente } from '../clientes/cliente';



export class Pedido{
    public id: number;
    public numero: number;
    public fecha: Date;
    public monto: number;
    public descripcion: string;
    public fechaEntrega: Date;
    public descuento: number;
    public fechaBaja: Date;
    public cliente: Cliente;
    public factura: any;
    public sucursal: any;
    public pedidoEntregaDetalleList: ListaProductos[];

}
