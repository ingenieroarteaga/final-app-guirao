//EN EL FRONT DE LA APP HAY UN MANEJO DE STOCK PARA REALIZAR PEDIDOS
//EN LA APP NO ESTÁ ESE MANEJO, PERO EL OBJETO QUE ENVIO TIENE QUE TENER
//COMO UNO DE LOS ATRIBUTOS "STOCK" Y DENDTRO DE STOCK ESTÁ LA INFO DEL
//PRODUCTO QUE QUEREMOS AGREGAR A LA LSITA

export class Sucursal {
    public id: number;
    public idFe: number;
    public descripcion: string;
    public fechaBaja: Date;
    public contactoSucursalList: any[];
    public domicilioSucursalList: any[];
    public facturaList: any[];
    public pedidoVentaList: any[];
    public stockList: any[];
    public pagoList: any[];
    public cajaList: any[];
    // public compraList: Compra[];
    // public presupuestoList: Presupuesto[];
    // public pedidoCompraList: PedidoCompra[];
    public empresa: any;

}
