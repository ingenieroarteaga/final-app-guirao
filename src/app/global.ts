export var GLOBAL = {
    // url: 'http://192.168.0.112:8080/'
    url: 'http://185.37.226.154:8080/guirao/',
    // url: 'http://localhost:8080/'
    // url: 'http://ea236153.ngrok.io/',

    // datos para getProductos
    urlProductos: 'http://185.37.226.154:8080/guirao/productos',
    // datos de marcas
    urlMarcas: 'http://185.37.226.154:8080/guirao/marcas',
    // datos de marcas
    urlFamiliaProd: 'http://185.37.226.154:8080/guirao/familias-productos',
    //Casa Chaca
    // urlProductos: 'http://ea236153.ngrok.io/productos',

    // datos para getTipos
    urlTipos: 'http://185.37.226.154:8080/guirao/tipos-productos',
    //Casa Chaca
    // urlProductos: 'http://ea236153.ngrok.io/productos',

    // datos para getTipos
    urlFamilias: 'http://185.37.226.154:8080/guirao/familias-productos',
    //Casa Chaca
    // urlProductos: 'http://ea236153.ngrok.io/productos',

    // datos para getMarcas
    urlMarcas: 'http://185.37.226.154:8080/guirao/marcas',
    //Casa Chaca
    // urlProductos: 'http://ea236153.ngrok.io/productos',


    // datos para getClientes
    urlClientes: 'http://185.37.226.154:8080/guirao/clientes',
    //Casa Chaca
    // urlClientes: 'http://ea236153.ngrok.io/clientes',

    // datos para crear pedidos
    urlPedidos: 'http://185.37.226.154:8080/guirao/pedido-entrega',
    //Casa Chaca
    // urlPedidos: 'http://ea236153.ngrok.io/pedido-venta',

    // datos para crear pedidos
    urlStocks: 'http://185.37.226.154:8080/guirao/stocks',
    //Casa Chaca
    // urlStocks: 'http://ea236153.ngrok.io/stocks',

    // datos para traer pedidos
    urlPedidosVentas: 'http://185.37.226.154:8080/guirao/pedidos-entregas',
    //Casa Chaca
    // urlStocks: 'http://ea236153.ngrok.io/stocks',

}
